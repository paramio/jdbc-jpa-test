package com.kovansys.benchmark;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;


@Component
public class JdbcBenchmarker extends Benchmarker{

	@Autowired
	JdbcTemplate jdbcTemplate;

	
	@Override
	public void insertAll() {
		for (int i = 0; i < ITERS; i++) {
			insert(new JpaEntity(i + 1, "test1", "test1", "test1", "test1"));
		}
	}
	
	
	@Override
	public void readAll() {
		List<JpaEntity> results = jdbcTemplate.query("select id, test1, test2, test3, test4 from jdbc_table",
				new RowMapper<JpaEntity>() {
					@Override
					public JpaEntity mapRow(ResultSet rs, int rowNum) throws SQLException {
						return new JpaEntity(rs.getInt("id"), rs.getString("test1"), rs.getString("test2"), rs
								.getString("test3"), rs.getString("test4"));
					}
				});
		
		results.isEmpty();
	}
	private void insert(JpaEntity test) {
		jdbcTemplate.update("INSERT INTO jdbc_table(id, test1, test2, test3, test4) values(?,?,?,?,?)", 
				test.getId(),
				test.getTest1(), test.getTest2(), test.getTest3(), test.getTest4());

	}

	@Override
	public void prepare() {
		String SQL = null;
		SQL = "CREATE TABLE IF NOT EXISTS `jdbc_table` (`id` int(11) NOT NULL, `test1` varchar(255) DEFAULT NULL,  `test2` varchar(255) DEFAULT NULL,  `test3` varchar(255) DEFAULT NULL,  `test4` varchar(255) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
		jdbcTemplate.execute(SQL);



						
	}


	@Override
	public void preInsert() {
		String SQL = "TRUNCATE TABLE jdbc_table";
		jdbcTemplate.execute(SQL);
		
	}
	

	/*
	public void test() {
		long startTime = System.currentTimeMillis();

		long endTime = System.currentTimeMillis();
		System.out.println(String.format("JDBC insert %f s", (float) (endTime - startTime) / 1000));

		testRepo.deleteAllInBatch();
		testRepo.flush();

		long startTime1 = System.currentTimeMillis();

		long endTime1 = System.currentTimeMillis();
		System.out.println(String.format("JPA insert %f s", (float) (endTime1 - startTime1) / 1000));

		long startTime3 = System.currentTimeMillis();
		List<JpaTable> invList = testRepo.findAll();
		long endTime3 = System.currentTimeMillis();

		System.out.println(String.format("JPA find all %f s", (float) (endTime3 - startTime3) / 1000));


		System.out.println(String.format("JDBC find all %f s", (float) (endTime2 - startTime2) / 1000));

	}
	*/ 

}
