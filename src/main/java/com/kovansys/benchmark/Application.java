package com.kovansys.benchmark;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication

public class Application implements CommandLineRunner {

	@Autowired
	JdbcBenchmarker jdbcBenchmarker;

	@Autowired
	JpaBenchmarker jpaBenchmarker;


	/*
	@Autowired
	ProfileExecutionAspect profileExecution;
	*/


	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		jdbcBenchmarker.run();
		jpaBenchmarker.run();

		System.out.println("Finished!");
	}
}
