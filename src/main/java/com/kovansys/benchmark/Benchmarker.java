package com.kovansys.benchmark;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;


public abstract class Benchmarker {
	
	public static final int ITERS = 10000;


	private static final Logger logger = LoggerFactory.getLogger("Benchmarker");

	
	public abstract void prepare();

	public abstract void preInsert();
	public abstract void insertAll();
	public abstract void readAll();
	

	public void run() {

		prepare();

		StopWatch stopWatch = new StopWatch(getClass().getName());
		
		
		
		stopWatch.start("Select Times");
		readAll();
		stopWatch.stop();
		

		preInsert();
		stopWatch.start("Insert Times");
		insertAll();
		stopWatch.stop();
		


		logger.info(stopWatch.prettyPrint());

	}

}
