package com.kovansys.benchmark;


import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Component
public class JpaBenchmarker extends Benchmarker {
	
	@Autowired
	JpaTableRepository jpaRepo;
	
	@PersistenceContext
	EntityManager em;

	@Override
	public void readAll() {
		jpaRepo.findAll();
	}

	@Override
	@Transactional
	public void insertAll() {
		for (int i = 0; i < ITERS; i++) {
			em.persist(new JpaEntity( "test1", "test1", "test1", "test1"));
		}
		
		em.flush();

	}

	@Override
	public void prepare() {
	}
	
	@Override
	@Transactional
	public void run() {
		super.run();
	}

	public JpaTableRepository getJpaRepo() {
		return jpaRepo;
	}

	public void setJpaRepo(JpaTableRepository jpaRepo) {
		this.jpaRepo = jpaRepo;
	}

	@Override
	public void preInsert() {
		jpaRepo.deleteAll();
		
	}
	


}
