package com.kovansys.benchmark;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaTableRepository extends JpaRepository<JpaEntity, Long> {

}
